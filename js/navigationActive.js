var loc = window.location.pathname;

$('.nav-header').find('a').each(function() {
  $(this).toggleClass('nav-header__link--activated', $(this).attr('href') == loc);
});