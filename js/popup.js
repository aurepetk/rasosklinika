$(window).load(function () {
    $(".trigger__popup--person").click(function(){
       $('.popup--person').show();
    });
    $('.popup--person').click(function(){
        $('.popup--person').hide();
    });
    $('.popup__close-icon').click(function(){
        $('.popup--person').hide();
    });

    $(".trigger__popup--schedule").click(function(){
        $('.popup--schedule').show();
     });
     $('.popup').click(function(){
         $('.popup--schedule').hide();
     });
     $('.popup__close-icon').click(function(){
         $('.popup--schedule').hide();
     });

     $(".trigger__popup--info").click(function(){
        $('.popup--info').show();
     });
     $('.popup').click(function(){
         $('.popup--info').hide();
     });
     $('.popup__close-icon').click(function(){
         $('.popup--info').hide();
     });
});