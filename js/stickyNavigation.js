$(document).ready(function() {
    $('.js--section-sticky').waypoint(function(direction) {
        if (direction == 'down') {
            $('.nav-header').addClass('sticky');
            $('.nav-header__link').addClass('sticky__link');
        } else {
            $('.nav-header').removeClass('sticky');
            $('.nav-header__link').removeClass('sticky__link');
        }
    });
});